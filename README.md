## Name
My version of roast_meter

## Description
Information on how I built here:
https://sites.google.com/view/coffee4randy/projects/roast-meter 

This is based on this project
https://github.com/juztins-lab/roast-meter 
I don't think its a fork because I used an ESP32 instead of their processor, and had to change the OLED library for the ESP32.


## Badges


## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Arduino Libraries Installed:

AsyncElegantOTA V 2.2.7 for Over the Air uploading, https://github.com/ayushsharma82/AsyncElegantOTA

TelnetStream V 1.2.2 for telenet for wireless debugging, https://github.com/jandrassy/TelnetStream

MAX3010x library https://github.com/sparkfun/SparkFun_MAX3010x_Sensor_Library 

esp oled library  https://github.com/ThingPulse/esp8266-oled-ssd1306 



## Usage
User's guide here https://github.com/juztins-lab/roast-meter/wiki/Roast-Meter-User%27s-Guide

"Initial results as of April 2023 indicate 300-500 microns as a grind range that generates repeatable readings on the Roast Meter. This may change as more data comes in."
Can use coffee grind app to determine grind size
 https://play.google.com/store/apps/details?id=com.rwurzer.burrista&hl=en_US&gl=US&pli=1 

## Support


## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
This is based on this project
https://github.com/juztins-lab/roast-meter 
I don't think its a fork because I used an ESP32 instead of their processor, and had to change the OLED library for the ESP32.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
