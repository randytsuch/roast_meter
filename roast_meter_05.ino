/*modified by Randy T
// ver 5 uses larger chars, display format changes
// ver 5 created SetLED variable to led power to calibrate sensor
// ver t adds FlipScreen bool to allow changing screen upside down viewing.  
// Changed letter positioning to support FlipScreen

//OLED is from seeedstudio https://wiki.seeedstudio.com/Grove-OLED-Display-0.66-SSD1306_v1.0/ 
//using ESP32 for processor board
//added wifi and Over The Air programming function with AsyncElegantOTA
// based on roast_meter VERSION 1.0.0-rc.3


*/

//************************** Libraries ***********************************************
//lib for wifi
#include <WiFi.h>

//lib for Over the Air (ota) programming
#include <ESPAsyncWebServer.h>  //https://github.com/me-no-dev/ESPAsyncWebServer
#include <AsyncElegantOTA.h>  //https://github.com/ayushsharma82/AsyncElegantOTA 

#include <Wire.h>
#include "MAX30105.h"
#include "SSD1306Wire.h"  // OLED Library, https://github.com/ThingPulse/esp8266-oled-ssd1306

#include <TelnetStream.h>  //for telenet for wireless debugging, https://github.com/jandrassy/TelnetStream

//Credentials.h are in same directory as this file
#include "Credentials.h" //login info, and other stuff to not make public

//************************** Parameters to Initialize ***********************************************
//Change SetLED to calibrate sensor 
//The target reading objective is 182-188 on the display
byte SetLED = 36;  //Options: 0=Off to 255=50mA

bool FlipScreen = true;  //change to flip screen orietation upside down on OLED, value = true or false

//*****************************************************************************************


//#define PIN_RESET 9
//#define DC_JUMPER 1

// for ota
const char* host = "esp32 Roaster";
// Create AsyncWebServer object on port 80
AsyncWebServer server(80);

// Create a WebSocket object
AsyncWebSocket ws("/ws");

MAX30105 particleSensor;

// Initialize the OLED display parameters are (i2c address, sda, scl, size)
SSD1306Wire display(0x3c, SDA, SCL, GEOMETRY_64_48);   // ADDRESS, SDA, SCL  -  SDA and SCL usually populate automatically based on your board's pins_arduino.h e.g. https://github.com/esp8266/Arduino/blob/master/variants/nodemcu/pins_arduino.h


long samplesTaken = 0;  //Counter for calculating the Hz or read rate
long unblockedValue;    //Average IR at power up
//long startTime;         //Used to calculate measurement rate
int calibratedReading;  //number displayed on OLED
int rLevel;             //scaled number read from particle sensor 

String multiplyChar(char c, int n) {
  String result = "";
  for (int i = 0; i < n; i++) {
    result += c;
  }
  return result;
}

//**************************** OLED display Function ***********************************

void displayMeasurement() {
  
  // clear the display
  display.clear();
  
  calibratedReading = f(rLevel);
  
  String calibratedReadingString = String (calibratedReading);
 // int centerPadding = 4 - String(calibratedReading).length();
 // String paddingText = multiplyChar(' ', centerPadding);

/*  if (FlipScreen == true) {
    display.setTextAlignment(TEXT_ALIGN_CENTER);
  }
  else { display.setTextAlignment(TEXT_ALIGN_CENTER);
  }
  */
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.setFont(ArialMT_Plain_24);  //10, 16 or 24 are valid sizes

  if (rLevel == 0) {
    //64x48
    display.drawString(20, 8, "  Load");//20 = hor, 8 = vertical
    display.display();      // write the buffer to the display
  }
  else {
//  display.drawString(32, 15, paddingText);
  display.drawString(20, 8, calibratedReadingString);
  display.display();
  }


  Serial.println("real:" + String(rLevel));
  Serial.println("agtron:" + String(calibratedReading));
  Serial.println("===========================");

}

//**************************** Calculate Value Function ***********************************

int f(int x) {
  int intersectionPoint = 117;
  float deviation = 0.165;

  return x - (intersectionPoint - x) * deviation;
}


//**************************** Convert to Hex Function ***********************************

byte intToHex(int num) {
  // Convert the integer to hexadecimal string
  String hexString = String(num, HEX);

  // Convert the hexadecimal string to byte
  byte hexByte = 0;
  for (int i = 0; i < hexString.length(); i++) {
    char c = hexString.charAt(i);

    if (isDigit(c)) {
      hexByte = (hexByte << 4) + (c - '0');
    } else {
      c = toUpperCase(c);
      hexByte = (hexByte << 4) + (c - 'A' + 10);
    }
  }

  return hexByte;
}


//**************************** Function to find wifi channel ***********************************

int32_t getWiFiChannel(const char *ssid) {
  if (int32_t n = WiFi.scanNetworks()) {
      for (uint8_t i=0; i<n; i++) {
          if (!strcmp(ssid, WiFi.SSID(i).c_str())) {
              return WiFi.channel(i);
          }
      }
  }
  return 0;
}


// **************************  Setup section . ***********************************
void setup() {
  Serial.begin(115200);

  Wire.begin();

  // Initialising the UI will init the display too.
  display.init();

if (FlipScreen == true) {
  display.flipScreenVertically();
}

  delay(100);  // Delay 100 ms

  // ------------ Setup Wi-Fi ------------------
  // Set device as a Wi-Fi Station and set channel
  WiFi.mode(WIFI_STA);

  int32_t channel = getWiFiChannel(ssid);
    WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
//    Serial.println("Setting as a Wi-Fi Station..");
  }
  Serial.print("ESP32 IP Address: ");
  Serial.println(WiFi.localIP());
  Serial.print("Wi-Fi Channel: ");
  Serial.println(WiFi.channel()); 

//  WiFi.printDiag(Serial); // Uncomment to verify channel number before

  //setup webserver for OTA
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(200, "text/plain", "Hi! I am ESP32. add /update for ota");
  });

  AsyncElegantOTA.begin(&server);    // Start ElegantOTA
  server.begin();
  Serial.println("HTTP server started");

  // Initialize sensor
  if (particleSensor.begin(Wire, I2C_SPEED_FAST) == false)  //Use default I2C port, 400kHz speed
  {
    Serial.println("MAX30105 was not found. Please check wiring/power. ");
    while (1)
      ;
  }
  
  // Variable used to calibrate your sensor, change the value below +/- 1 till you get an unbleached V60 paper to read between 180-190 values on the screen.
  byte ledBrightness = intToHex(SetLED);  //Options: 0=Off to 255=50mA

  byte sampleAverage = 32;  //Options: 1, 2, 4, 8, 16, 32
  byte ledMode = 2;         //Options: 1 = Red only, 2 = Red + IR, 3 = Red + IR + Green
  int sampleRate = 3200;    //Options: 50, 100, 200, 400, 800, 1000, 1600, 3200
  int pulseWidth = 411;     //Options: 69, 118, 215, 411
  int adcRange = 4096;      //Options: 2048, 4096, 8192, 16384

  particleSensor.setup(ledBrightness, sampleAverage, ledMode, sampleRate, pulseWidth, adcRange);  //Configure sensor with these settings

  //particleSensor.setPulseAmplitudeRed(0);    //Turn off Red LED
  particleSensor.setPulseAmplitudeGreen(0);  //Turn off Green LED


  Serial.print(unblockedValue);
  //startTime = millis();

  // Update to ignore readings under 30.000
  unblockedValue = 30000;
}

// **************************  Main Loop . ***********************************

void loop() {
  samplesTaken++;
  long currentDelta = particleSensor.getIR() - unblockedValue;

  if (currentDelta > (long)100) {
    rLevel = int(particleSensor.getIR()) / 1000;
//    displayMeasurement(rLevel);
    displayMeasurement();
} 
  else {
    rLevel=0;
    displayMeasurement();      
//    displayMeasurement(0);
  }
  delay(500);  //used to be 100, increase so updates very 1/2 sec
}
